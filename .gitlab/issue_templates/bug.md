<!--

Thank you for reporting a bug in guerrilla-radio. 

Before you open a ticket:
* Make sure you've checked out our Troubleshooing page on the wiki:
  https://gitlab.com/adam949/guerrilla-radio/-/wikis/troubleshooting
* If you have a question, you can ask in our Matrix channel at
  #guerrilla-radio:hax0rbana.org
* Search for an existing ticket and if one is found, add a thumbs up (:+1:) to
  indicate that you are affected. We use this in prioritizing issues.
* Please check whether the bug can be reproduced with the latest release.

The fastest way to fix a bug is to open a Merge Request, if you are able.
  * https://gitlab.com/adam949/guerrilla-radio/-/merge_requests

-->

# Issue description
<!-- In a sentence or two, explain the issue you're facing -->

# Steps to reproduce
List the minimal actions needed to reproduce the behavior.

1. ...
2. ...
3. ...

## Expected behavior:
Describe what you expected to happen.

## Actual behavior:
Describe what actually happened.

# Environment
<!-- Describe your hardware, operating system, and desired configuration (e.g.
     a node connected to two mesh networks over wifi, acting as a gateway over
     ethernet, and serving up an access point on a third wifi card) -->

* Hardware:
* Operating system/distro:
* Configuration:
* Kernel: Run `uname -a` and copy the output here
