<!--

Thank you for suggesting an idea to improve guerrilla-radio

If you want to discuss your idea before opening a ticket, you can join our
Matrix channel at #guerrilla-radio:hax0rbana.org

* Search for an existing ticket and if one is found, add a thumbs up (:+1:) to
  indicate that you are also interested in that feature. We use this in
  prioritizing which tickets to work on.

-->

# Proposal
Short summary of the feature.

# Current behavior
Describe what currently happens.

# Desired behavior
Describe what you want.

# Alternatives considered
Describe other solutions or features you considered.

# Use case
Why is this important (helps with prioritizing requests)?
