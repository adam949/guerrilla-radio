#!/bin/sh -e
#
# This script is called by bridge_ap.service after hostapd.service is started.
# The interface can not be bridged until hostapd sets it up as an access point.
#

# First, make sure the bridge exists. It should, but it doesn't hurt to be sure
/usr/local/bin/create_bridge.sh

if [ -f /etc/network/interfaces.d/4_ap ]; then
	ap_iface=`grep auto /etc/network/interfaces.d/4_ap | sed 's/auto //'`
	# if our AP isn't already bridged, bridge it now
	bridge link show dev $ap_iface | grep -wq 'master br0' || ip link set dev $ap_iface master br0
fi
