# Demo website
This is just a silly demo website to verify things are working as expected. To
serve it up, cd into this directory and run `ip a; python3 -m http.server`.

That will show you your current IP address and serve up the webpage on port
8000. If a gateway is connected to the network your computer is on, the mesh
nodes (and anyone connected to an access point running on a mesh node) should be
able to get to this website.
