# Overview
This is a shell script to turn any Debian-based computer into a gateway, AP, or
pure mesh node.

When machine is joined to a mesh, it can immediately communicate with other
computers in the mesh. If there's a gateway that is connected to the internet,
everyone will be online. Mesh networks are an excellent solution for if there's
a disruption in cell service, or some Internet Service Providers (ISPs) are
offline.

# Quickstart
Check out this repo on the computer you want to set up and run `mesh_me_up.sh`
The `mesh_me_up.sh` script will use the internet to install dependencies
(dhcpcd). If these are already installed then `mesh_me_up.sh` will not require
an internet connection.

```sh
sudo apt install -y git
git clone https://gitlab.com/adam949/guerrilla-radio
cd guerrilla-radio
sudo ./mesh_me_up.sh
```

Take a look at the top of the `mesh_me_up.sh` script to see the options you can
pass to it.

# How to help
- Set up a mesh network and report any issues you run into
  - [Issue tracker](https://gitlab.com/adam949/guerrilla-radio/-/issues)
  - [Matrix chatroom](https://element.hax0rbana.org/#/room/#guerrilla-radio:hax0rbana.org) (#guerrilla-radio:hax0rbana.org)
- Tell others about the project
- Join the [Matrix chatroom](https://element.hax0rbana.org/#/room/#guerrilla-radio:hax0rbana.org) and help others
- Add information to [the wiki](https://gitlab.com/adam949/guerrilla-radio/-/wikis/home)
- Take a look at the [open tickets](https://gitlab.com/adam949/guerrilla-radio/-/issues)

# Node Types
There are three basic node types in a mesh network.

- Gateway - Connection to the mesh and other networks (e.g. the Internet)
- Access Point (AP) - Connects to the mesh and serves up an access point
- Pure Mesh - Connects to the mesh only

A node could connect to the mesh, act as a gateway, and act as an access point.
Mobile devices and laptops are generally going to connect to a Gateway.  It's
possible to add your laptop to the mesh network by running `mesh_me_up.sh`, but
that will make persistent changes that may make it cumbersome to go back to
joining normal access points in the future.

IP addresses are typically assigned by the network to which the gateway is
connected.  The mesh just extends that network.

It's not only possible to have multiple gateways in a single mesh network, but
it's desirable.  If one internet connection goes down, the network will
automatically adapt and keep as many nodes online as possible, and route traffic
through the fastest internet connection available.

Multiple mesh networks can be joined together by having a node that is connected
to each network and bridges them together. Losing any node in the mesh network
should result in a minimal loss in network coverage.

# Roadmap

## Phase 1
Easy to spin up a mesh network that is connected to the internet and can provide
service in emergencies, or just in everyday life.

Expectation is to typically have at least one node connected to the internet.

## Phase 2
Make it easy to set up standalone services that can run without any connection
to the internet:

- DNS
- Kiwix
- Matrix
- Jitsi
- Briar
- Gitea/Gitlab
- etc.

## Phase 3
Bridging different networks together (ham packet radio, LoRaWAN, GoTenna mesh,
and whatever else we can rig to work with this).

# Resouces
- [Revision control & issue tracker](https://gitlab.com/adam949/guerrilla-radio)
- [Wiki](https://gitlab.com/adam949/guerrilla-radio/-/wikis/home) for links to
  other howtos, background information, etc.
- #guerrilla-radio:hax0rbana.org on
  [Matrix](https://element.hax0rbana.org/#/room/#guerrilla-radio:hax0rbana.org)
  for real time text chat
- [Video Conferencing](https://jitsi.hax0rbana.org/guerrilla-radio) for real
  time voice/video/screen collaboration
