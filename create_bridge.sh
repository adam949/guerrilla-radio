#!/bin/sh
#
# We intentionally ignore errors here, as this script will be called
# multiple times and if it fails because the interface already exists,
# that's not really a problem.
#

# This will make sure the bridge interface exists and is up
ip link show dev br0 > /dev/null 2>&1 || ip link add name br0 type bridge
ip link set dev br0 up
