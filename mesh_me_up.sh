#!/bin/bash -e
###
# Script should be run as root on the computer that should be configured for
# the mesh.  The configuration files will be updated and applied.
#
# If an Access Point (AP) is not set up with a second wifi adapter, clients
# that are not mesh-aware will not be able to connect.
####

prg_name="$(basename -- "$0")"

usage() {
cat << EOF
Usage:
    $prg_name <option>... # run as root

Options:
    -a, --ap-interface <interface>: network interface to use for the AP, if it
        exists (default: wlan1).
    -c, --mesh-channel <channel number>: channel to use for the mesh connection
        (default: 1).
    -C, --ap-channel <channel number>: channel to use for the AP (default: 11).
    -i, --mesh-interface <interface>: network interface to use for the mesh
        (default: wlan0).
    -e, --ethernet-interface <interface>: network interface of ethernet interface
        (default: eth0).
    -m, --mtu <MTU>: the MTU for the mesh network (default: 1500). Use 1532 if
        all of the interfaces in your mesh support it.
    -s, --ssid <SSID>: the SSID of the mesh network (default: guerrilla-radio).
    -t, --type <node type>: "server" if the node has an Internet connection;
        "client" for a node that will just be on the mesh (default: client).
    --help: display this help message and exit.
EOF
}

# Defaults
ap_interface="wlan1"
ap_channel=11
mesh_interface="wlan0"
mesh_channel=1
eth_interface="eth0"
mesh_mtu="1500"
mesh_ssid="guerrilla-radio"
node_type="client"

# Arguments
if ! args=`getopt -n "$prg_name" -l help,ap-interface:,mesh-channel:,ap-channel:,mesh-interface:,ethernet-interface:,mtu:,ssid:,type: -- a:c:C:i:e:m:s:t: "$@"`; then
	usage >&2
	exit 1
fi
eval set -- "$args"
opt_parse=true
while [ $# -gt 0 ]; do
	if $opt_parse; then
		case "$1" in
		--)
			opt_parse=false ;;
		--help)
			usage
			exit ;;
		-a | --ap-interface)
			ap_interface="$2"
			shift ;;
		-c | --mesh-channel)
			mesh_channel="$2"
			shift ;;
		-C | --ap-channel)
			ap_channel="$2"
			shift ;;
		-e | --ethernet-interface)
			eth_interface="$2"
			shift ;;
		-i | --mesh-interface)
			mesh_interface="$2"
			shift ;;
		-m | --mtu)
			mesh_mtu="$2"
			shift ;;
		-s | --ssid)
			mesh_ssid="$2"
			shift ;;
		-t | --type)
			node_type="$2"
			shift ;;
		esac
	fi
	shift
done

if [ `whoami` != "root" ]; then
	echo "Script must be run as root to change the network configs"
	exit 1
fi

# Modify the config to get dhcpcd to ignore a network interface
function denydhcp {
	# If we don't have a denyinterfaces line, we add it eith the new interface
	grep -q "^denyinterfaces" /etc/dhcpcd.conf || echo "denyinterfaces $1" | tee -a /etc/dhcpcd.conf > /dev/null
	# If our denyinterfaces line doesn't have the new interface, we add it
	grep -q "^denyinterfaces.*$1" /etc/dhcpcd.conf || sed "s/\(^denyinterfaces .*\)/\1 $1/" -i /etc/dhcpcd.conf > /dev/null
}

# Make sure rfkill is not blocking any wifi interfaces
blocked=`rfkill -o ID,TYPE,SOFT list | grep wlan | grep "\bblocked" | awk '{print $1}'`
for i in $blocked; do
	rfkill unblock $i
done
# and make sure any services won't blocked them in the future either
systemctl stop systemd-rfkill.service systemd-rfkill.socket || true
systemctl disable systemd-rfkill.service systemd-rfkill.socket || true

# Make sure dhcpcd and batctl are installed
which dhcpcd > /dev/null || DEBIAN_FRONTEND=noninteractive apt-get install -y dhcpcd
which batctl > /dev/null || DEBIAN_FRONTEND=noninteractive apt-get install -y batctl

# Make sure the kernel module is loaded and will be loaded in the future
sudo modprobe batman-adv
if [ -d /etc/modules-load.d ]; then
	echo batman-adv | sudo tee /etc/modules-load.d/batman-adv.conf
else
	grep batman-adv /etc/modules || echo batman-adv | sudo tee -a /etc/modules
fi

# We don't want to get DHCP addresses on the mesh interface, as that's just the
# underlying transport.  IP addresses will be assigned to bat0, which rides on
# top of the physical interface.
denydhcp $mesh_interface

# Copy over the script that creates the bridge interface, which may be used by
# several interfaces later
cp create_bridge.sh /usr/local/bin/

# Set up the the bat0 interface to use the mesh interface
cp mesh_interface mesh_interface.tmp
sed -i 's/^\(\s\+wireless-essid\s\+\).*$/\1'"$mesh_ssid/" mesh_interface.tmp
sed -i 's/^\(\s*wireless-channel\s\+\).*$/\1'"$mesh_channel/" mesh_interface.tmp
sed -i "s/wlan0/$mesh_interface/" mesh_interface.tmp
sed -i "s/mtu .*/mtu $mesh_mtu/" mesh_interface.tmp
mv mesh_interface.tmp /etc/network/interfaces.d/1_mesh

# Set up the bat0 interface for this node type
cp bat0 bat0.tmp
sed -i "s/wlan0/$mesh_interface/" bat0.tmp
sed -i "s/ client/ $node_type/" bat0.tmp
sed -i "s/mtu .*/mtu $((mesh_mtu-32))/" bat0.tmp
mv bat0.tmp /etc/network/interfaces.d/3_bat0
denydhcp bat0

# If we have a LAN interface, bridge it
if ip link show $eth_interface > /dev/null 2> /dev/null; then
	# Set up the ethernet port to be bridged (to the bat0 interface)
	cp eth_interface eth_interface.tmp
	sed -i "s/eth0/$eth_interface/" eth_interface.tmp
	mv eth_interface.tmp /etc/network/interfaces.d/2_eth
	# We get the DHCP address on the bridge interface, not on $eth_interface
	denydhcp $eth_interface
fi

# Make sure the mesh has the correct MTU for the bat0 interface, even if if a
# DHCP server claims that it should be larger.  The MTU for the bat0 interface
# should be 32 bytes smaller than the physical interface on which it is
# operating.  This is because the mesh uses a 32 byte header which is going to
# use up some of every available frame.
if [ -f /etc/dhcpcd.exit-hooks ]; then\
	# If there's an existing exit-hook, we append to it if necessary
	echo "ip link set dev bat0 mtu $((mesh_mtu-32))" | tee -a /etc/dhcpcd.exit-hook
else
	cp dhcpcd.exit-hook dhcpcd.exit-hook.tmp
	sed -i "s/mtu .*/mtu $((mesh_mtu-32))/" dhcpcd.exit-hook.tmp
	mv dhcpcd.exit-hook.tmp /etc/dhcpcd.exit-hook
fi
chmod +x /etc/dhcpcd.exit-hook

# If the Wi-Fi interface to be used as an AP exists, set it up as an AP:
# bridge it to br0 as well, so that'll be the same network.  The IP addresses
# will not be assigned by us, but rather by the upstream DHCP server.
if ip link show "$ap_interface" > /dev/null 2> /dev/null; then
	# Install hostapd if we don't already have it
	which hostapd > /dev/null || DEBIAN_FRONTEND=noninteractive apt-get install -y hostapd

	# Copy over the script & service that will bridge the AP to the mesh
	cp bridge_ap.sh /usr/local/bin/
	cp bridge_ap.service /etc/systemd/system/
	systemctl enable bridge_ap.service

	# Set up hostapd
	cp access_point.conf /etc/hostapd/hostapd.conf
	sed -i "s/wlan1/$ap_interface/g" /etc/hostapd/hostapd.conf
	sed -i 's/^\(\s*channel\s*=\s*\).*$/\1'"$ap_channel/" /etc/hostapd/hostapd.conf
	systemctl unmask hostapd.service

	# Make sure the AP interface doesn't get a DHCP address
	denydhcp "$ap_interface"
fi

echo "Restarting the network interfaces"
systemctl restart networking.service
